@extends('layouts.app')

@section('title', 'Subreddits')

@section('content')
    <form method="get">
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Subreddit</th>
                <th>No of Posts</th>
                <th>Action</th>
            </tr>
        </thead>
            <tbody>
            <tr>
                <td></td>
                <td><input type="text" name="subreddit" id="subreddit" value="{{$qSub}}"> </td>
                <td><input type="text" name="post_count" id="post_count" value="{{$postCount}}"></td>
                <td><input type="Submit" name="submit" value="Search"></td>
            </tr>
            @if (count($subReddits)> 0)
                @foreach ($subReddits as $sub)
                    <tr>
                        <td>{{$sub->subreddit_id}}</td>
                    <td><a href="{{ URL::to('/index.php/sub-posts/' . $sub->subreddit_id) }}">{{$sub->subreddit}}</a></td>
                    <td>{{$sub->taxonomy_count}}</td>
                        <th><a href="{{ URL::to('/index.php/sub-posts/' . $sub->subreddit_id) }}">View</a></th>
                    </tr>
                @endforeach


            @else
                <tr colspan ='2'>No Subreddit found.</tr>
                 @endif
            </tbody>
    </table>
    </form>
    {{ $subReddits->links() }}
@endsection
