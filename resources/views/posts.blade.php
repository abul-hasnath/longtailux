@extends('layouts.app')

@section('title', "Subreddit Posts")
<a href="/">Home</a>
@section('content')
    <form method="get">
    <table>

        @if (count($posts)> 0)
            <thead>
            <tr>
                <th>#</th>
                <th>Post ID</th>
                <th>Score</th>
                <th>Permalink</th>
                <td>&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><input  type="text" name="db_id" id="db_id" value="{{$db_id}}"> </td>
                <td><input  type="text" name="reddit_id" id="reddit_id" value="{{$reddit_id}}"> </td>
                <td><input  type="text" name="score" id="score" value="{{$score}}"> </td>
                <td><input  type="text" name="permalink" id="permalink" value="{{$permalink}}"> </td>
                <td><input  type="submit" name="Submit" id="Submit" value="Search"> </td>
            </tr>
            @foreach ($posts as $post)
                <tr>
                    <td>{{$post->subreddit_post_id}}</td>
                    <td>{{$post->post->reddit_post_id}}</td>
                    <td>{{$post->post->score}}</td>
                    <td>{{$post->post->permalink}}</td>
                    <td>&nbsp;</td>
                </tr>
            @endforeach
            </tbody>
        @else
            <span>Could not find Posts for this Subreddit.</span>

        @endif

    </table>
    </form>
    {{ $posts->links() }}
@endsection
