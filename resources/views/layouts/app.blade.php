<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #000;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 1rem;
            }
            table td, table th{
                padding: 5px 20px;
                max-width: 300px;
                min-width: 22px;
                text-align: left;
            }
            ul.pagination{
                display:flex;
                list-style: none;
            }
            ul.pagination li{
                padding-right: 30px;
            }
        </style>

    </head>
    <body>
        <h1>
            @yield('title')
        </h1>

        @yield('content')
    </body>
</html>
