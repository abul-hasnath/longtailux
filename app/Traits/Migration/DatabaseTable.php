<?php
declare(strict_types=1);

namespace App\Traits\Migration;

use Illuminate\Support\Facades\Schema;

/**
 * Class DatabaseTable
 * @package App\Traits\Migration
 */
trait DatabaseTable
{
    /**
     * @param string $tableName
     * @return bool
     */
    public function tableExists(string $tableName): bool {
        return Schema::hasTable($tableName);
    }

    /**
     * @param string $tableName
     * @param string $columnName
     * @return bool
     */
    public function columnExists(string $tableName, string $columnName): bool{
        return Schema::hasColumn($tableName, $columnName);
    }
}