<?php
declare(strict_types=1);

namespace App\Traits\Migration;


use Illuminate\Support\Facades\Schema;

/**
 * Class DatabaseIndex
 * @package App\Traits\Migration
 */
trait DatabaseIndex
{
    /**
     * @param string $tableName
     * @param string $indexName
     * @return bool
     */
    public function indexExists(string $tableName, string $indexName):bool
    {
            $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $schemaManager->listTableDetails($tableName);

            return $doctrineTable->hasIndex($indexName);
    }

}