<?php
declare(strict_types=1);

namespace App\Traits\Http;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait GuzzleRequest
{
    /**
     * @param string $url
     * @return string
     * @throws \Exception
     */
    public function saveJsonToFile(string $url):string {

        $filename   = Storage::disk('feedStorage')->path(''). time(). '-getJson.json';
        $client     = new \GuzzleHttp\Client(['timeout' => 2, 'headers' => ['Accept-Encoding' => 'gzip']]);

        try{
            $response = $client->request('GET', $url, ['sink' => $filename]);

            if($response->getStatusCode() == 200){
                return $filename;
            }else{
                throw new \Exception('Unable to read feed...', 1001);
            }

        }catch (\Exception $e){
            Log::emergency($e->getMessage());
            throw $e;
        }

    }

}