<?php
declare(strict_types=1);

namespace App\Providers\Database;

use Illuminate\Support\ServiceProvider;
use App\Services\SubRedditTaxonomyService;
use App\Services\Interfaces\SubRedditTaxonomyServiceInterface;


class SubRedditTaxonomyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SubRedditTaxonomyServiceInterface::class, SubRedditTaxonomyService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
