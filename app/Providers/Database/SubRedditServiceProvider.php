<?php
declare(strict_types=1);

namespace App\Providers\Database;

use Illuminate\Support\ServiceProvider;
use App\Services\SubRedditService;
use App\Services\Interfaces\SubRedditServiceInterface;


class SubRedditServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SubRedditServiceInterface::class, SubRedditService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
