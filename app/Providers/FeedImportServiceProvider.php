<?php
declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Interfaces\FeedImportRepositoryInterface;
use App\Repositories\JsonFeedImportRepository;
use App\Services\SubRedditService;
use App\Services\PostService;
use App\Services\SubRedditTaxonomyService;

class FeedImportServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FeedImportRepositoryInterface::class, function ($app) {
            return new JsonFeedImportRepository(
                new SubRedditService(),
                new PostService(),
                new SubRedditTaxonomyService());
        });
        $this->app->bind('jsonFeed', function ($app) {
            return new JsonFeedImportRepository(
                new SubRedditService(),
                new PostService(),
                new SubRedditTaxonomyService());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
