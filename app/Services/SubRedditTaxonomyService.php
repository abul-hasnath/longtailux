<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Taxonomy;
use App\Services\Interfaces\SubRedditTaxonomyServiceInterface;
use Illuminate\Support\Facades\Config;

/**
 * Class SubRedditTaxonomyService
 * @package App\Services
 */
class SubRedditTaxonomyService implements SubRedditTaxonomyServiceInterface
{
    /**
     * @param array $data
     * @return Taxonomy
     */
    public function save(array $data): Taxonomy{
        $taxonomy = New Taxonomy;
        $taxonomy->subreddit_id = $data['subreddit_id'];
        $taxonomy->subreddit_post_id = $data['subreddit_post_id'];
        $taxonomy->save();
        return $taxonomy;
    }

    /**
     * @param int $subRedditId
     * @param int $dbId
     * @param $redditId
     * @param int $score
     * @param $permalink
     * @return mixed
     */
    public function getTaxonomyWithPosts(int $subRedditId, int $dbId, $redditId, int $score, $permalink){

        $itemsPerPage   = Config::get('pagination.itemsPerListPage');
        $clause         = [];
        $clause[]       = ['subreddit_id', '=', $subRedditId];

        if($dbId){
            $clause[]   = ['subreddit_post_id', '=', $dbId];
        }
        $query          = Taxonomy::where($clause)->with('post');

        if($redditId || $score || $permalink){

            $query->whereHas('post', function ($q) use($redditId, $score, $permalink) {

                $clause2        = [];

                if($redditId){
                    $clause2[]  = ['reddit_post_id', '=', $redditId];
                }
                if($score){
                    $clause2[]  = ['score', '=', $score];
                }
                if($permalink){
                    $clause2[]  = ['permalink', 'like', '%%' . $permalink . '%%'];
                }
                $q->where($clause2);
            });
        }

        return $query->simplePaginate($itemsPerPage);
    }
}