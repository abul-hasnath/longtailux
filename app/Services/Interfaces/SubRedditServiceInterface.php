<?php


namespace App\Services\Interfaces;

use App\Models\Subreddit;

interface SubRedditServiceInterface
{
    /**
     * @param array $data
     * @return Subreddit
     */
    public function save(array $data): Subreddit;

    /**
     * @param $subreddit
     * @param int $postCount
     * @return mixed
     */
    public function getAllWithPostCount($subreddit, int $postCount);
}