<?php


namespace App\Services\Interfaces;

use App\Models\Taxonomy;

interface SubRedditTaxonomyServiceInterface
{
    /**
     * @param array $data
     * @return Taxonomy
     */
    public function save(array $data): Taxonomy;

    /**
     * @param int $subRedditId
     * @param int $dbId
     * @param $redditId
     * @param int $score
     * @param $permalink
     * @return mixed
     */
    public function getTaxonomyWithPosts(int $subRedditId, int $dbId, $redditId, int $score, $permalink);
}