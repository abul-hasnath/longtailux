<?php
declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Config;
use App\Models\Subreddit;
use App\Services\Interfaces\SubRedditServiceInterface;

/**
 * Class SubRedditService
 * @package App\Services
 */
class SubRedditService implements SubRedditServiceInterface
{
    /**
     * @param array $data
     * @return Subreddit
     */
    public function save(array $data): Subreddit {
        return Subreddit::firstOrCreate(['subreddit' => trim($data['subreddit'])]);
    }

    /**
     * @param $subreddit
     * @param int $postCount
     * @return mixed
     */
    public function getAllWithPostCount($subreddit, int $postCount){

        $itemsPerPage = Config::get('pagination.itemsPerListPage');

        $clause = [];

        if($subreddit){
            $clause[] = ['subreddit', 'like', '%%'.$subreddit.'%%'];
        }

        $query = Subreddit::where($clause)->withCount('taxonomy');

        if($postCount > 0){
            $query->has('taxonomy', '=', $postCount);
        }

        return $query->simplePaginate($itemsPerPage);
    }
}