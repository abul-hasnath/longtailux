<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Post;

/**
 * Class PostService
 * @package App\Services
 */
class PostService
{
    /**
     * @param array $data
     * @return Post
     */
    public function save(array $data): Post {
        $post                   = new Post;
        $post->reddit_post_id   = trim($data['id']);
        $post->permalink        = trim($data['permalink']);
        $post->author           = trim($data['author']);
        $post->created_utc      = $data['created_utc'];
        $post->score            = $data['score'];
        $post->save();

        return $post;
    }
}