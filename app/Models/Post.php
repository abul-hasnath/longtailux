<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'subreddit_posts';
    protected $primaryKey = 'subreddit_post_id';
    public $timestamps = false;

    public function taxonomy()
    {
        return $this->hasOne('App\Models\Taxonomy', 'subreddit_post_id', 'subreddit_post_id');
    }
}
