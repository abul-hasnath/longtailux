<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subreddit extends Model
{
    protected $table = 'subreddit';

    protected $primaryKey = 'subreddit_id';

    protected $fillable = ['subreddit'];

    public $timestamps = false;

    public function taxonomy(){
        return $this->hasMany('App\Models\Taxonomy', 'subreddit_id', 'subreddit_id');
    }
}
