<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Taxonomy extends Model
{
    protected $table = 'subreddit_taxonomy';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo('App\Models\Post', 'subreddit_post_id', 'subreddit_post_id');
    }
    public function subreddit()
    {
        return $this->belongsTo('App\Models\Subreddit', 'subreddit_id', 'subreddit_id');
    }
}
