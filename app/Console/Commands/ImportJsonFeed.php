<?php
declare(strict_types=1);

namespace App\Console\Commands;

use JsonFeed;
use Illuminate\Console\Command;
use App\Traits\Http\GuzzleRequest;

class ImportJsonFeed extends Command
{
    use GuzzleRequest;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importFeed:json {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import JSON data feed from URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $filename = $this->saveJsonToFile($this->argument('url'));

            JsonFeed::setFileLocation($filename);
            JsonFeed::saveObjectsToDatabase();

        }catch(\Exception $e){
            echo $e->getMessage();
        }

    }
}
