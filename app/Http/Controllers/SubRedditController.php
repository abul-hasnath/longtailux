<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\Interfaces\SubRedditServiceInterface;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

/**
 * Class SubRedditController
 * @package App\Http\Controllers
 */
class SubRedditController extends BaseController
{
    private $subRedditService;

    function __construct(SubRedditServiceInterface $subRedditService){

        $this->subRedditService = $subRedditService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request){

        $subreddit = $request->input('subreddit');
        $postCount = $request->input('post_count');
        $subreddits =$this->subRedditService->getAllWithPostCount($subreddit, (int)$postCount);

        return view('subreddit', ['subReddits' => $subreddits, 'postCount' => $postCount, 'qSub' => $subreddit]);
    }
}