<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\Interfaces\SubRedditTaxonomyServiceInterface;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

/**
 * Class SubRedditPostController
 * @package App\Http\Controllers
 */
class SubRedditPostController extends BaseController
{
    private $subRedditTaxonomyService;

    function __construct(SubRedditTaxonomyServiceInterface $subRedditTaxonomyService)
    {
        $this->subRedditTaxonomyService = $subRedditTaxonomyService;
    }

    /**
     * @param $subRedditId
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list($subRedditId, Request $request){

        $dbId       = $request->input('db_id');
        $redditId   = $request->input('reddit_id');
        $score      = $request->input('score');
        $permalink  = $request->input('permalink');
        $posts      = $this->subRedditTaxonomyService->getTaxonomyWithPosts((int)$subRedditId, (int)$dbId, $redditId, (int)$score, $permalink);

        return view('posts', ['posts' => $posts,
            'db_id' => $dbId,
            'reddit_id' => $redditId,
            'score' => $score,
            'permalink' => $permalink]);
    }

}