<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

/**
 * Interface FeedImportRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface FeedImportRepositoryInterface
{
    /**
     * @param string $pathToFile
     * @return void
     */
    public function setFileLocation(string $pathToFile);

    public function saveObjectsToDatabase();

}