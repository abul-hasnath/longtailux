<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Repositories\Interfaces\FeedImportRepositoryInterface;
use App\Services\SubRedditService;
use App\Services\PostService;
use App\Services\SubRedditTaxonomyService;

/**
 * Class JsonFeedImportRepository
 * @package App\Repositories
 */
class JsonFeedImportRepository implements FeedImportRepositoryInterface
{

    private $_pathToFile;
    private $subRedditService;
    private $postService;
    private $taxonomyService;

    /**
     * JsonFeedImportRepository constructor.
     * @param SubRedditService $subRedditService
     * @param PostService $postService
     * @param SubRedditTaxonomyService $taxonomyService
     */
    function __construct(SubRedditService $subRedditService, PostService $postService, SubRedditTaxonomyService $taxonomyService)
    {
        $this->subRedditService     = $subRedditService;
        $this->postService          = $postService;
        $this->taxonomyService      = $taxonomyService;
    }

    /**
     * @param string $pathToFile
     */
    private function _checkFile(string $pathToFile){
        throw_if(!is_readable($pathToFile), new \Exception("Unable to read file location", 2001));
    }

    /**
     * @param string $pathToFile
     */
    public function setFileLocation(string $pathToFile)
    {
        $this->_checkFile($pathToFile);

        $this->_pathToFile = $pathToFile;
    }

    /**
     * Save R=feed objects to database
     */
    public function saveObjectsToDatabase()
    {
        throw_if(!$this->_pathToFile, new \Exception("Set file location using setFileLocation()", 2002));

        $parser = new \JsonCollectionParser\Parser();

        $parser->parse($this->_pathToFile, function (array $item){

            $subreddit  = $this->subRedditService->save($item);

            throw_if(!isset($subreddit->subreddit_id), new \Exception("Unable to lookup/Save subreddit", 2003));

            $post       = $this->postService->save($item);
            $this->taxonomyService->save(['subreddit_id' => $subreddit->subreddit_id, 'subreddit_post_id' => $post->subreddit_post_id]);
        });

    }

}