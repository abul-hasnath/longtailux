<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Traits\Migration\DatabaseTable;

class CreateRedditTables extends Migration
{
    use DatabaseTable;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!$this->tableExists('subreddit')) {

            Schema::create('subreddit', function (Blueprint $subReddit) {

                $subReddit->engine = 'InnoDB';

                $subReddit->string('subreddit', 255);
                $subReddit->integer('subreddit_id')->autoIncrement();
            });

        }

        if (!$this->tableExists('subreddit_posts')) {
            Schema::create('subreddit_posts', function (Blueprint $posts) {

                $posts->engine = 'InnoDB';

                $posts->integer('subreddit_post_id')->autoIncrement();
                $posts->string('reddit_post_id', 45);
                $posts->string('permalink', 500);
                $posts->string('author', 500);
                $posts->integer('created_utc');
                $posts->tinyInteger('score');
            });
        }

        if (!$this->tableExists('subreddit_taxonomy')) {
            Schema::create('subreddit_taxonomy', function (Blueprint $taxonomy) {

                $taxonomy->engine = 'InnoDB';

                $taxonomy->integer('id')->autoIncrement();
                $taxonomy->integer('subreddit_id');
                $taxonomy->integer('subreddit_post_id');

            });

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subreddit_taxonomy');
        Schema::dropIfExists('subreddit');
        Schema::dropIfExists('subreddit_posts');
    }
}
