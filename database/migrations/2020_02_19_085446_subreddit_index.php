<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Traits\Migration\DatabaseIndex;
use App\Traits\Migration\DatabaseTable;

class SubredditIndex extends Migration
{
    private $_tableName = 'subreddit_taxonomy';
    private $_indexName = 'idx_subreddit_posts';

    use DatabaseIndex;
    use DatabaseTable;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ($this->tableExists($this->_tableName)) {
            Schema::table($this->_tableName, function(Blueprint $taxonomy){
                if(!$this->indexExists($this->_tableName, $this->_indexName))
                $taxonomy->index(['subreddit_id', 'subreddit_post_id'], $this->_indexName);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if ($this->tableExists($this->_tableName)) {
            Schema::table($this->_tableName, function(Blueprint $taxonomy){

                if($this->indexExists($this->_tableName, $this->_indexName)){
                    $taxonomy->dropIndex($this->_indexName);
                }
            });
        }
    }
}
