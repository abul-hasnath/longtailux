<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Traits\Migration\DatabaseTable;

class UpdatePostScoreField extends Migration
{
    use DatabaseTable;

    private $_tableName = 'subreddit_posts';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ($this->tableExists($this->_tableName)) {
            Schema::table($this->_tableName, function(Blueprint $posts){
                if($this->columnExists($this->_tableName, 'score'))
                    $posts->smallInteger('score')->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ($this->tableExists($this->_tableName)) {
            Schema::table($this->_tableName, function(Blueprint $posts){
                if($this->columnExists($this->_tableName, 'score'))
                    $posts->tinyInteger('score')->change();
            });
        }
    }
}
