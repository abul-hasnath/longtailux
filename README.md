# README #

Laravel implementation of candidate task from Longtail UX
### Setup ###

* Use git clone command or download the repository in your local environment
* Use cd command to enter source code directory
* Execute `composer install` to install dependencies
* Execute `composer dump-autoload` to generate class-map cache
* Rename `.env.example` to `.env`, then set the necessary environment variables

### Database Migration ###

* Use `php artisan migrate` to install database
* `2020_02_20_142605_update_post_score_field.php (optional)` is there to fix an issue in the database schema requirement. It was a requirement that, `score` field in `subreddit_posts` should be one byte tiny integer. However, due to range limitations, this field is unable to hold `score` value of incoming field. This migration sets `score` field to have two byte.

### Artisan command for JSON feed import ###

* Execute `php artisan importFeed:json {url}` to import JSON feed into database. For example, `php artisan FeedImport:json http://files.pushshift.io/reddit/comments/sample_data.json`
* This command does not truncate the relevant tables before it starts importing; Please use 'php artisan migrate:refresh' to empty the database before successive executions.
